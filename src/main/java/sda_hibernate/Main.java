package sda_hibernate;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

        public static void main(String[] args) {

            try (Connection connection = prepareConnection()){
                Place newPlace = askUserForPlace();
                addPlace(connection,newPlace);
                List<Place> places = getPlaces(connection);
                places.forEach(System.out::println);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    private static Place askUserForPlace() {
        Place place = new Place();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj miasto: ");
        place.city = scanner.nextLine();

        System.out.println("Podaj adres: ");
        place.address = scanner.nextLine();

        System.out.println("Podaj nazwę: ");
        place.name = scanner.nextLine();

        System.out.println("Czy na miejscu jest szatnia (tak): ");
        place.cloakRoom = "tak".equals(scanner.nextLine().toLowerCase());

        System.out.println("Czy na miejscu jest parking (tak): ");
        place.parking = "tak".equals(scanner.nextLine().toLowerCase());

        return place;
    }

    private static void addPlace(Connection connection, Place place) throws SQLException {

        String sqlQuery = "insert into place (city,address,name,cloak_room,parking) values (?,?,?,?,?);";
        // to jest metoda przygotowujaca zapytanie
        // korzystanie z zapytan ktore beda mialy parametry z metody
        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setString(1,place.city);
        statement.setString(2,place.address);
        statement.setString(3,place.name);
        statement.setBoolean(4,place.cloakRoom);
        statement.setBoolean(5,place.parking);
        statement.executeUpdate();

        statement.close();
    }

    private static List<Place> getPlaces(Connection connection) throws SQLException {
        String sqlQuery = "select * from place;";

        //obiekt Statement do wywolywania zapytan
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sqlQuery);

        //metoda next() na pobranie wszystkich wierszy spelniajace zapytanie sql

        //obiekty typu place zbieramy w liscie
        List<Place> places = new ArrayList<>();
        while(resultSet.next()){
            Place place = new Place();

            //metoda na pobranie danych z kolumny trzeba znac zwracany typ
            place.id = (resultSet.getInt("id"));
            place.city = (resultSet.getString("city"));
            place.address = (resultSet.getString("address"));
            place.name = (resultSet.getString("name"));
            place.cloakRoom = (resultSet.getBoolean("cloak_room"));
            place.parking = (resultSet.getBoolean("parking"));

            //obiekt z pobranymi danymi zapisujemy w liscie
            places.add(place);

        }

        resultSet.close();
        statement.close();

        return places;
    }

    private static Connection prepareConnection() throws SQLException {
    String user = "root";
    String password = "Widzewek1910";
    // [informacja z jaka baza sie laczymy]:[z jaka baza sie laczymy]://[host]:[numer_portu]/
    // [nazwa bazy danych]?[strefaCzasowa]=[podajemy strefe]
    String url = "jdbc:mysql://localhost:3306/sda_hibernate?serverTimezone=Europe/Warsaw";
//    Connection connection = DriverManager.getConnection(url,user,password);
    return DriverManager.getConnection(url,user,password);
}

}

